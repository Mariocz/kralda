# /bin/bash


# Deploy script for Nette application
#
#	config.local.neon replaced by config.production.neon on production server
# 

# setup 
server='k-architekt.cz'
user='k-architekt.cz'
remote_dir='web'
key_path='/home/mario/.ssh/websupport_key'
rsync_exclude=('/www/images/uploads/*')
temp_dir='temp'

# index.php excluded from project files sync and send last because of maintenance mode
common_exclude=('*.ssh' '/temp/*' '/log/*' '/nbproject' '/.git' 'deploy.sh' '*~'\
    '.gitignore' 'composer.json' '/www/index.php' \
    'config.local.neon' 'config.production.neon')
    
# download remote index.php to set maintenance mode
function downloadIndex {
    scp -q -i $key_path $user@$server:$remote_dir/www/index.php $temp_dir/
}
    
function displayHelp {
    echo 'Deploy script for Nette application' 
    echo '-----------------------------------'
    echo -e '-h or --help\t\t display this help'
    echo -e '-d\t\t\t delete remote logs and exceptions'
    echo -e '--delete-cache\t\t deletes temp/cache contents WITHOUT DEPLOY'
    echo -e '--toggle-maintenance\t turns on/off maintenance mode on remote server WITHOUT DEPLOY'
}

function toggleMaintenance {
    downloadIndex
    if [ -f $temp_dir/index.php ]
    then
    state=''
        if grep -Eq "require '\.maintenance\.php'" $temp_dir/index.php
          then
            sed -ri "/require '\.maintenance\.php';/d" $temp_dir/index.php
            state='disabled'                        
          else
            sed -ri "/^(<\?php|<\?)/a require '.maintenance.php';" $temp_dir/index.php
            state='enabled'
        fi  
        rsync -az \
        $temp_dir/index.php $user@$server:$remote_dir/www/
        rm $temp_dir/index.php
        echo "*** Maintenance mode $state"
    else
        echo "!!! No index on remote server ($user@$server:$remote_dir/www/index.php)"
        exit 1
    fi
}


# check options
delete_logs=0
while getopts ":dh-:" OPT; do
  case $OPT in
    d)
      delete_logs=1
;;
    h)
      displayHelp
      exit 0
;;
    -)
        case $OPTARG in #case for long switches
            "toggle-maintenance"*)
                toggleMaintenance
                exit 0
        ;;
            "delete-cache"*)
                toggleMaintenance              
                mkdir $temp_dir/empty
                rsync -az --delete \
                $temp_dir/empty/ $user@$server:$remote_dir/temp/cache/
                toggleMaintenance
                rmdir $temp_dir/empty
                echo '*** cache deleted'                
                exit 0
        ;;
            "help"*)
                displayHelp
                exit 0
        ;;
                  ?*)
                 echo "!!! Wrong argument '$OPTARG', stopping process"
                 echo
                 displayHelp
                 exit 1
        ;;
        esac 
;;
    ?)
             echo "!!! Wrong argument '$OPT', stopping process"
             echo
             displayHelp
             exit 1
;;
  esac
done

current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $current_dir

# set maintenance mode
echo '* setting maintenance mode on remote app'
downloadIndex
# set maintance mode if app is on remote
if [ -f $temp_dir/index.php ] 
then
    if grep -Eq "require '\.maintenance\.php'" $temp_dir/index.php
    then
        echo "  i maintenance mode was already on"
        echo "      after deploy state depends on local index."
    else
        toggleMaintenance
    fi
    
fi

# create exclude text file
for i in "${rsync_exclude[@]}"
do
echo $i >> exclude.txt
done
for i in "${common_exclude[@]}"
do
echo $i >> exclude.txt
done


echo "* Starting rsync to $server:$remote_dir"
echo "   * sendig project files"
rsync -az \
    --delete \
    --filter "P log/*.log" --filter "P log/*.html" --filter "P /.htaccess" \
    --include ".htaccess" --exclude-from exclude.txt \
    . $user@$server:$remote_dir/

rm exclude.txt


if [ -f app/config/config.production.neon ]
then
	echo '   * sending production config'
	cp app/config/config.production.neon $temp_dir/config.local.neon
	rsync -az \
	    $temp_dir/config.local.neon $user@$server:$remote_dir/app/config/
	rm $temp_dir/config.local.neon
else
	echo '   i Production config not found'
fi
echo '   * sending index.php'

rsync -az \
www/index.php $user@$server:$remote_dir/www/

echo '* rsync completed'

if [ $delete_logs -eq 1 ]
then
    echo '* removing logs and stored exceptions'
    sftp -q -i $key_path $user@$server >/dev/null 2>&1 << SFTPDELIM
         cd $remote_dir
         rm log/*.log
         rm log/*.html
         bye
SFTPDELIM
fi

echo '*** Deploy completed! Yeeeeah! ***'

exit 0
