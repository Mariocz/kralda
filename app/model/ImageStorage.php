<?php

namespace App\Model;
use Nette\Image;

class ImageStorage extends \Nette\Object {

	/**
	 *
	 * @var \App\Model\ImageRepository
	 */
	private $images;
	private $uploadDir;

	public function __construct($uploadDir, \App\Model\ImageRepository $images) {
		$this->uploadDir = $uploadDir;
		$this->images = $images;
	}
	
	public function setMainImage($projectId, $imageId) {
		$this->getProjectImages($projectId)->where('main_image', 1)->update(['main_image'=>0]);
		$this->images->findById($imageId)->update(['main_image'=>1]);
	}
	
	/**
	 * 
	 * @param int $projectId
	 */
	public function getProjectImages($projectId) {
		return $this->images->findAll()->where('project_id', $projectId)->order('main_image DESC');
	}

	/**
	 * 
	 * @param string $title
	 * @param \Nette\Http\FileUpload $image
	 * @param \Nette\Http\FileUpload $preview
	 * @param int $projectId
	 */
	public function saveImage($title, $image, $preview, $projectId) {
		$values = array(
		    'title' => $title,
		    'project_id' => $projectId
		);

		$this->images->beginTransaction();
		$inserted = $this->images->insert($values);

		$newImgName = $inserted->id . '_' . $image->sanitizedName;
		$newPrevName = 'p' . $inserted->id . '_' . $preview->sanitizedName;
		$fileValues = array(
		    'name' => $newImgName,
		    'preview_name' => $newPrevName
		);
		$inserted->update($fileValues);
		$this->images->commit();
		
		$previewImage = $preview->toImage();
		
		$previewImage->resize(325, 230, Image::FILL)->crop('50%', '50%', 325, 230);

		$image->move($this->uploadDir . '/' . $newImgName);
		$previewImage->save($this->uploadDir . '/' . $newPrevName);
	}

	/**
	 * 
	 * @param int $imageId
	 */
	public function deleteImage($imageId) {
		$image = $this->images->findById($imageId);
		$imageFile = $this->uploadDir . '/' . $image->name;
		$previewFile = $this->uploadDir . '/' . $image->preview_name;
		if (file_exists($imageFile))
			unlink($imageFile);
		if (file_exists($previewFile))
			unlink($previewFile);
		$image->delete();
	}

	/**
	 * 
	 * @param int $projectId
	 */
	public function deleteAllProjectImages($projectId) {
		$projectImages = $this->images->findAll()->where('project_id', $projectId)->fetchPairs('name', 'preview_name');

		foreach ($projectImages as $name => $preview_name) {
			$imageFile = $this->uploadDir . '/' . $name;
			$previewFile = $this->uploadDir . '/' . $preview_name;
			if (file_exists($imageFile))
				unlink($imageFile);
			if (file_exists($previewFile))
				unlink($previewFile);
		}
		
		$this->images->findAll()->where('project_id', $projectId)->delete();
	}

}