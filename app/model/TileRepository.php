<?php

namespace App\Model;

use Nette;

class TileRepository extends Nette\Object {

	/** @var Nette\Database\Context */
	private $database;

	const TABLE = 'mainpage_tiles';

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}
	
	/** @return Nette\Database\Table\Selection */
	public function findAll() {
		return $this->database->table(self::TABLE);
	}

	/** @return Nette\Database\Table\ActiveRow */
	public function findById($id) {
		return $this->findAll()->get($id);
	}
	
	public function changeTile($tile, $image) {
		$this->findAll()->where('id', $tile)->update(array('image_id' => $image));
	}

}