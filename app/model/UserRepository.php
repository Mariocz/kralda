<?php

namespace App\Model;

use Nette;

class UserRepository extends Nette\Object {

	/** @var Nette\Database\Context */
	private $database;

	const TABLE = 'users';

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}

	/** @return Nette\Database\Table\Selection */
	public function findAll() {
		return $this->database->table(self::TABLE);
	}

	/** @return Nette\Database\Table\ActiveRow */
	public function findById($id) {
		return $this->findAll()->get($id);
	}

	/** @return Nette\Database\Table\ActiveRow */
	public function insert($values) {
		return $this->findAll()->insert($values);
	}

}