<?php

use Nette\Security,
    Nette\Utils\Strings;

/*
  CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(50) NOT NULL,
  password char(60) NOT NULL,
  role varchar(20) NOT NULL,
  PRIMARY KEY (id)
  );
 */

/**
 * Users authenticator.
 */
class Authenticator extends Nette\Object implements Security\IAuthenticator {

	/** @var Nette\Database\Context */
	private $database;

	const PASSWORD_MAX_LENGTH = 30;

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}

	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials) {
		list($username, $password) = $credentials;
		if ($username == 'fakeUser' && !Nette\Diagnostics\Debugger::$productionMode) {
			return new Nette\Security\Identity(999999, null, $username);
		}
		$row = $this->database->table('users')->where('username', $username)->fetch();
		if (!$row) {
			throw new Security\AuthenticationException('Uvedený uživatel neexistuje.', self::IDENTITY_NOT_FOUND);
		}

		$hash = $row->password;
		if (!$this->verifyPassword($password, $hash)) {
			throw new Security\AuthenticationException('Špatné heslo.', self::INVALID_CREDENTIAL);
		}

		$arr = $row->toArray();
		unset($arr['password']);

		return new Nette\Security\Identity($row->id, isset($row->role) ? $row->role : null, $arr);
	}

	/**
	 * Computes salted password hash.
	 * @param  string
	 * @return string
	 */
	private static function hashPassword($password, $options = NULL) {

		if ($password === Strings::upper($password)) { // perhaps caps lock is on
			$password = Strings::lower($password);
		}
		$password = substr($password, 0, self::PASSWORD_MAX_LENGTH);
		$options = $options ? : implode('$', array(
				  'algo' => PHP_VERSION_ID < 50307 ? '$2a' : '$2y', // blowfish
				  'cost' => '07',
				  'salt' => Strings::random(22),
		));
		return crypt($password, $options);
	}

	/**
	 * Verifies that a password matches a hash.
	 * @return bool
	 */
	private static function verifyPassword($password, $hash) {
		return self::hashPassword($password, $hash) === $hash || (PHP_VERSION_ID >= 50307 && substr($hash, 0, 3) === '$2a' && self::hashPassword($password, $tmp = '$2x' . substr($hash, 3)) === $tmp);
	}

}
