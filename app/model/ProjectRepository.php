<?php

namespace App\Model;

use Nette;

class ProjectRepository extends Nette\Object {

	/** @var Nette\Database\Context */
	private $database;

	const TABLE = 'projects';
	const TILE_TABLE = 'main_page_tiles';
	private $categories = array(
		    'drobna_architektura',
		    'interier',
		    'rodinne_domy',
		    'obcanske_stavby',
		    'urbanismus'
		);

	public function __construct(Nette\Database\Context $database) {
		$this->database = $database;
	}

	/** @return Nette\Database\Table\Selection */
	public function findAll() {
		return $this->database->table(self::TABLE);
	}

	/** @return Nette\Database\Table\ActiveRow */
	public function findById($id) {
		return $this->findAll()->get($id);
	}
	
	/** 
	 * @param string $category category name
	 * @return Nette\Database\Table\ActiveRow */
	public function findByCategory($category) {
		if(in_array($category, $this->categories))
			return $this->findAll()->where('category', $category);
		throw new Nette\InvalidArgumentException("Wrong category '$category'");
	}

	/** @return Nette\Database\Table\ActiveRow */
	public function insert($values) {
		return $this->findAll()->insert($values);
	}
	
	/** @return Nette\Database\Table\ActiveRow */
	public function updateMainpageTile($tile, $project, $picture) {
		$data = array(
		    'project_id' => $project,
		    'picture_id' => $picture
		);
		return $this->database->table(self::TILE_TABLE)->where('id', $tile)->update($data);
	}

}