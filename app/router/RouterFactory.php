<?php

use Nette\Application\Routers\RouteList,
    Nette\Application\Routers\Route;

/**
 * Router factory.
 */
class RouterFactory {

	/**
	 * @return Nette\Application\IRouter
	 */
	public function createRouter() {
		$router = new RouteList();
		$router[] = new Route('sitemap.xml', 'Homepage:sitemap');
		$router[] = new Route('kontakt[/]', 'Homepage:contact');
		$router[] = new Route('profil[/]', 'Homepage:profil');
		$router[] = new Route('projekty/[<category>]', 'Project:default');
		$router[] = new Route('projekty/[<category>]/<project>', 'Project:detail');
		$router[] = new Route('admin[/]', 'Admin:projects');
		$router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
		return $router;
	}

}