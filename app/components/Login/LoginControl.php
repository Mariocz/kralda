<?php

class LoginControl extends \Nette\Application\UI\Control {

	public function render() {

		$this->template->setFile(__DIR__ . '/login.latte');
		$this->template->render();
	}

	protected function createComponentLoginForm() {

		$form = new LoginForm;
		$form['ok']->onClick[] = callback($this, 'loginFormOkClicked');
		if (!Nette\Diagnostics\Debugger::$productionMode) {
			$form['fakeLogin']->onClick[] = callback($this, 'loginFormFakeClicked');
		}
		return $form;
	}

	public function loginFormOkClicked(\Nette\Forms\Controls\SubmitButton $button) {

		try {
			$user = $this->presenter->user;
			$user->setExpiration('14 days', FALSE);
			$values = $button->form->getValues();
			$user->login($values->username, $values->password);
			$this->presenter->redirect('admin:');
		} catch (\Nette\Security\AuthenticationException $e) {
			$this->flashMessage($e->getMessage(), 'error');
			$button->form['password']->value = '';
		}
	}

	public function loginFormFakeClicked() {
		$this->presenter->user->login('fakeUser', '');
		$this->presenter->redirect('admin:');
	}

}