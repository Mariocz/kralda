<?php

class LoginForm extends Nette\Application\UI\Form {

    public function __construct() {

        parent::__construct();

        $this->build();
    }

    private function build() {
        $this->addText('username', 'Uživatelské jméno:');
        $this->addPassword('password', 'Heslo:');
        $this->addSubmit('ok', 'Vstoupit');
        if (!Nette\Diagnostics\Debugger::$productionMode) {
            $this->addSubmit('fakeLogin', 'fakeLogin');
        }
    }

}