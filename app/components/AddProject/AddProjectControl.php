<?php

use Nette\Application\UI\Form;
use Nette\Application\UI\Control;

interface IAddProjectControlFactory {

	/** @return AddProjectControl */
	function create();
}

class AddProjectControl extends Control {

	/**
	 *
	 * @var App\Model\ProjectRepository
	 */
	private $projects;

	/**
	 *
	 * @var \App\Model\ImageStorage
	 */
	private $images;
	private $uploadDir;

	public function __construct($uploadDir, App\Model\ProjectRepository $projects, App\Model\ImageStorage $images) {
		$this->uploadDir = $uploadDir;
		$this->projects = $projects;
		$this->images = $images;
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/addProject.latte');
		$this->template->render();
	}

	/**
	 * 
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentAddProjectForm() {
		$categories = array(
		    'drobna_architektura' => 'drobná arch.',
		    'interier' => 'interiér',
		    'rodinne_domy' => 'rodinné domy',
		    'obcanske_stavby' => 'občanské stavby',
		    'urbanismus' => 'urbanismus'
		);

		$form = new Form;
		$form->addSelect('category', null, $categories)
			   ->setPrompt('Vyberte kategorii')
			   ->setRequired('Vyberte kategorii projektu');
		$form->addText('name', null, 26, 26)
			   ->setRequired('Vyplňte název projektu')
			   ->setAttribute('class', 'with-prompt')
			   ->setAttribute('title', 'Název projektu');
		$form->addTextArea('description', null)
			   ->setRequired('Vyplňte popis projektu')
			   ->addRule(Form::MAX_LENGTH, 'Popis projektu musí být kratší než %d znaků', 1200)
			   ->setAttribute('cols', 40)
			   ->setAttribute('rows', 24)
			   ->setAttribute('class', 'with-prompt')
			   ->setAttribute('title', 'Popis projektu');

		// image upload inputs
		$images = $form->addContainer("images");
		for ($i = 1; $i < 13; $i++) {

			$form->addGroup("imageGroup$i");

			$sub = $images->addContainer("image$i");
			$sub->currentGroup = $form->currentGroup;

			$sub->addText('title', null . null, 26)
				   ->setAttribute('class', 'with-prompt')
				   ->setAttribute('title', 'Titulek obrázku');
			$sub->addUpload("preview", "Miniatura (325x230 pixelů)")
				   ->addCondition(Form::FILLED)
				   ->addRule(Form::IMAGE, "Miniatura obrázku $i nelze nahrát, obrázky nahrávejte ve formátu JPEG, PNG nebo GIF")
				   ->addRule($this->fileExtensionValidator, "Neplatná přípona miniatury obrázeku $i. Soubor musí mít příponu 'jpeg/png/gif'", array('jpeg', 'jpg', 'png', 'gif'));
			$sub->addUpload("image", "Velký obrázek")
				   ->addCondition(Form::FILLED)
				   ->addRule(Form::IMAGE, "Velký obrázek $i nelze nahrát, obrázky nahrávejte ve formátu JPEG, PNG nebo GIF")
				   ->addRule($this->fileExtensionValidator, "Neplatná přípona velkého obrázeku $i. Soubor musí mít příponu 'jpeg/png/gif'", array('jpeg', 'jpg', 'png', 'gif'));
		}

		$form->setCurrentGroup();
		$form->addSubmit('submitButton', 'Přidat projekt')
			   ->onClick[] = $this->addProjectFormSubmited;
		return $form;
	}

	public function addProjectFormSubmited($button) {


		$values = $button->getForm()->getValues();
		$projectValues = array(
		    'name' => $values->name,
		    'description' => $values->description,
		    'category' => $values->category
		);

		$projectId = $this->projects->insert($projectValues)->id;

		foreach ($values->images as $num => $imageContainer) {
			$image = $imageContainer->image;
			$preview = $imageContainer->preview;
			if ($image->isOK() && $preview->isOK()) {
				$this->images->saveImage($imageContainer->title, $image, $preview, $projectId);
			} else {
				if ($image->error == 1)
					$this->flashMessage("Soubor obrázeku $num byl příliš velký a nebyl nahrán", 'error');
				if ($preview->error == 1)
					$this->flashMessage("Soubor miniatury $num byl příliš velký a nebyl nahrán", 'error');
			}
		}
		
		$this->flashMessage('Projekt úspěšně přidán');
		$this->redirect('this');
	}
	
	/**
	 *
	 * @param Nette\Forms\IControl $control
	 * @param array $allowedExtensions lowercase! seznam povolenych pripon
	 * @return bool
	 */
	public function fileExtensionValidator(Nette\Forms\IControl $control, array $allowedExtensions) {
		$file = $control->getValue();

		if ($file instanceof \Nette\Http\FileUpload) {
			$ext = strtolower(pathinfo($file->getName(), PATHINFO_EXTENSION));
			return in_array($ext, $allowedExtensions);
		}

		return false;
	}
	
}