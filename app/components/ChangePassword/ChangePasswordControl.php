<?php

use Nette\Application\UI\Form;
use Nette\Application\UI\Control;

interface IChangePasswordControlFactory {

	/** @return ChangePasswordControl */
	function create();
}

class ChangePasswordControl extends Control {

	/**
	 * 
	 * @param App\Model\UserRepository $users
	 * @param \Nette\Security\IAuthenticator $authenticator
	 */
	
	private $users;
	private $authenticator;

	public function __construct(App\Model\UserRepository $users, \Nette\Security\IAuthenticator $authenticator) {
		$this->users = $users;
		$this->authenticator = $authenticator;
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/changePassword.latte');
		$this->template->render();
	}

	/**
	 * 
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentChangePassForm() {
		$form = new Form;
		$form->addPassword('oldPass')
			   ->setRequired('Vyplňte staré heslo')
			    ->setAttribute('class', 'with-prompt')
			   ->setAttribute('title', 'Staré heslo');
		$form->addPassword('newPass')
			   ->setRequired('Vyplňte nové heslo')
			    ->setAttribute('class', 'with-prompt')
			   ->setAttribute('title', 'Nové heslo');
		$form->addPassword('newPassConfirm')
			   ->setRequired('Vyplňte znovu nové heslo')
			   ->addRule(Form::EQUAL, 'Nová hesla se neschodují', $form['newPass'])
			    ->setAttribute('class', 'with-prompt')
			   ->setAttribute('title', 'Potvrďte nové heslo');
		
		$form->addSubmit('changePass', 'Změnit heslo')
			   ->onClick[] = $this->changePassSubmitted;

		return $form;
	}

	public function changePassSubmitted($button) {

		$values = $button->getForm()->getValues();

		$userId = $this->presenter->user->getId();

		$user = $this->users->findById($userId);
		if ($this->authenticator->verifyPassword($values->oldPass, $user->password)) {
			$values->newPass = $this->authenticator->hashPassword($values->newPass);
			$user->update(array('password' => $values->newPass));
			$this->flashMessage('Heslo úspěšně změněno');
		} else {
			$this->flashMessage('Chybně vyplněné staré heslo', 'error');
		}
		$this->redirect('this');			
	}

}