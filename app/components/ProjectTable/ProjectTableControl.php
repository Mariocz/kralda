<?php

use Nette\Application\UI\Form;
use Nette\Application\UI\Control;

interface IProjectTableControlFactory {

	/** @return ProjectTableControl */
	function create();
}

class ProjectTableControl extends Control {

	/**
	 *
	 * @var App\Model\ProjectRepository
	 */
	private $projects;

	/**
	 *
	 * @var App\Model\ImageStorage
	 */
	private $images;
	private $uploadDir;
	private $editId;

	public function __construct($uploadDir, App\Model\ProjectRepository $projects, \App\Model\ImageStorage $images) {

		$this->uploadDir = $uploadDir;
		$this->projects = $projects;
		$this->images = $images;
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/projectTable.latte');
		$this->getData();
		$this->template->render();
	}

	public function getData() {
		$this->template->tableHeaders = array('Název', 'Popis', 'Kategorie');
		$this->template->projects = $this->projects->findAll()->order('id DESC');
	}

	protected function createComponentEditForm() {

		$row = $row = $this->projects->findById($this->editId);

		$categories = array(
		    'drobna_architektura' => 'drobná arch.',
		    'interier' => 'interiér',
		    'rodinne_domy' => 'rodinné domy',
		    'obcanske_stavby' => 'občanské stavby',
		    'urbanismus' => 'urbanismus'
		);

		$form = new Form;
		$form->addHidden('id');
		$form->addSelect('category', null, $categories)
			   ->setPrompt('Vyberte kategorii')
			   ->setRequired('Vyberte kategorii projektu');
		$form->addText('name', null, 26, 26)
			   ->setRequired('Vyplňte název projektu')
			   ->setAttribute('class', 'with-prompt')
			   ->setAttribute('title', 'Název projektu');
		$form->addTextArea('description', null)
			   ->setRequired('Vyplňte popis projektu')
			   ->addRule(Form::MAX_LENGTH, 'Popis projektu musí být kratší než %d znaků', 1200)
			   ->setAttribute('cols', 35)
			   ->setAttribute('rows', 24)
			   ->setAttribute('class', 'with-prompt')
			   ->setAttribute('title', 'Popis projektu');

		if ($row)
			$form->setDefaults($row);

		$form->addSubmit('submitButton', 'Uložit projekt');
		$form->onSuccess[] = function ($button) {

				   $values = $button->getForm()->getValues();
				   $projectValues = array(
					  'name' => $values->name,
					  'description' => $values->description,
					  'category' => $values->category
				   );

				   $this->projects->findById($values->id)->update($projectValues);

				   $this->flashMessage('Projekt upraven');

				   $this->presenter->redirect('this');
			   };
		return $form;
	}
	
	public function actionEdit() {
		$this->template->imageEdit = false;
		$this->template->addImage = false;
		$this->template->selectImage = false;
	}

	public function handleDelete($id) {

		$row = $this->projects->findById($id);

		if ($row) {
			$this->images->deleteAllProjectImages($id);
			$row->delete();
			$this->flashMessage('Projekt smazán');
		} else {
			$this->flashMessage('Projekt se nepodařilo smazat', 'error');
		}

		$this->presenter->redirect('this');
	}

	public function handleEdit($id) {
		$this->editId = $this->template->editId = $id;
		$this->template->imageEdit = false;
		$this->template->selectImage = false;
		$this->template->editId = $id;
		$this->invalidateControl('sEditForm');
	}

	public function handleEndEdit() {
		$this->invalidateControl('sEditForm');
	}

	public function handleEditImages($editId) {
		$this->template->editId = $editId;
		$this->template->imageEdit = true;
		$this->template->addImage = false;
		$this->template->selectImage = false;
		$this->template->images = $this->images->getProjectImages($editId)
			   ->fetchPairs('id', 'preview_name');
		$this->invalidateControl('sEditForm');
	}

	public function handleSelectMainImage($editId) {
		$this->template->editId = $editId;
		$this->template->selectImage = true;
		$this->template->imageEdit = false;
		$this->template->addImage = false;
		$this->template->images = $this->images->getProjectImages($editId)
			   ->fetchPairs('id', 'preview_name');
		$this->invalidateControl('sEditForm');
	}
	
	public function handleSetMainImage($imageId, $editId) {
		$this->images->setMainImage($editId, $imageId);
		$this->flashMessage('Obrázek náhledu vybrán');
		$this->presenter->redirect('this');
	}
	
	public function handleDeleteImage($imageId, $editId) {
		$this->template->editId = $editId;
		$this->template->imageEdit = true;
		$this->template->addImage = false;
		$this->images->deleteImage($imageId);
		$this->template->images = $this->images->getProjectImages($editId)
			   ->fetchPairs('id', 'preview_name');
		$this->invalidateControl('sEditForm');
		$this->flashMessage('Obrázek smazán');
		$this->presenter->redirect('this');
	}

	public function handleAddImage($editId) {
		$this->editId = $this->template->editId = $editId;
		$this->template->imageEdit = true;
		$this->template->addImage = true;
		$this->invalidateControl('sEditForm');
	}

	protected function createComponentAddImageForm() {
		$form = new Form;
		$form->addHidden('id', $this->editId);
		$form->addText('title', null . null, 26)
			   ->setAttribute('class', 'with-prompt')
			   ->setAttribute('title', 'Titulek obrázku');
		$images = $form->addContainer("images");
		for ($i = 1; $i < 4; $i++) {

			$form->addGroup("imageGroup$i");

			$sub = $images->addContainer("$i");
			$sub->currentGroup = $form->currentGroup;

			$sub->addText('title', null . null, 26)
				   ->setAttribute('class', 'with-prompt')
				   ->setAttribute('title', 'Titulek obrázku');
			$sub->addUpload("preview", "Miniatura (325x230 pixelů)");
			$sub->addUpload("image", "Velký obrázek");
		}

		$form->addSubmit('submitButton', 'Přidat obrázky')
			   ->onClick[] = function ($button) {
				   $values = $button->getForm()->getValues();

				   foreach ($values->images as $num => $imageContainer) {
					   $image = $imageContainer->image;
					   $preview = $imageContainer->preview;
					   if ($image->isOK() && $preview->isOK()) {
						   $this->images->saveImage($imageContainer->title, $image, $preview, $values->id);
						   $this->flashMessage("Obrázek $num přidán");
					   } else {
						   switch ($image->error) {
							   case 1:
								   $this->flashMessage("Soubor obrázku $num je příliš velký", 'error');

								   break;
							   case 4:
								   $this->flashMessage("Soubor obrázku $num nebyl vybrán", 'error');
								   break;
							   default:
								   $this->flashMessage("Chyba nahrávání obrázku $num. Obrázky nahrávejte ve formátu GIF, PNG nebo JPEG", 'error');
								   break;
						   }
					   }
				   }

				   $this->presenter->redirect('this');
			   };
		//$form['submit']->getControlPrototype()->class[] = 'ajax';
		return $form;
	}

	/**
	 *
	 * @param Nette\Forms\IControl $control
	 * @param array $allowedExtensions lowercase! seznam povolenych pripon
	 * @return bool
	 */
	public function fileExtensionValidator(Nette\Forms\IControl $control, array $allowedExtensions) {
		$file = $control->getValue();

		if ($file instanceof \Nette\Http\FileUpload) {
			$ext = strtolower(pathinfo($file->getName(), PATHINFO_EXTENSION));
			return in_array($ext, $allowedExtensions);
		}

		return false;
	}

}