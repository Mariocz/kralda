<?php

use Nette\Application\UI\Form;
use Nette\Application\UI\Control;

interface IChangeTileControlFactory {

	/** @return ChangeTileControl */
	function create($tileId);
}

class ChangeTileControl extends Control {

	/** @var App\Model\TileRepository $tiles */
	private $tiles;
	/** @var App\Model\ProjectRepository $projects */
	private $projects;
	/** @var App\Model\ImageRepository $images */
	private $images;
	
	/** @var int $tileId Id of tile being edited */
	private $tileId;

	/**
	 * 
	 * @param App\Model\TileRepository $tiles
	 * @param App\Model\ProjectRepository $projects
	 * @param \App\Model\ImageRepository $images
	 * @param int $tileId tile number
	 */
	public function __construct($tileId,
		   App\Model\TileRepository $tiles, 
		   App\Model\ProjectRepository $projects,
		   App\Model\ImageRepository $images) {
		$this->tileId = $tileId;
		$this->tiles = $tiles;
		$this->projects = $projects;
		$this->images = $images;
	}

	public function render() {
		$this->template->setFile(__DIR__ . '/changeTile.latte');
		$this->template->_form = $this['changeTileForm'];
		$this->template->render();
	}

	/**
	 * 
	 * @return \Nette\Application\UI\Form
	 */
	protected function createComponentChangeTileForm() {
		$categories = array(
		    'drobna_architektura' => 'drobná arch.',
		    'interier' => 'interiér',
		    'rodinne_domy' => 'rodinné domy',
		    'obcanske_stavby' => 'občanské stavby',
		    'urbanismus' => 'urbanismus'
		);

		$form = new Form;
		$form->addSelect('category', null, $categories)
			   ->setPrompt('Vyberte kategorii');
		$form->addSelect('project', null)
			   ->setPrompt('Vyberte projekt')
			   ->setAttribute('style', 'display: none');
		return $form;
	}

	public function handleCategorySelected($value) {
		if ($value) {
			$projects = $this->projects->findByCategory($value)->fetchPairs('id', 'name');

			$this['changeTileForm']['project']
				   ->setItems($projects)
				   ->setAttribute('style', '');
		} else {
			$this['changeTileForm']['project']
				   ->setItems(array())
				   ->setAttribute('style', 'display: none');
		}
		$this->invalidateControl('imagesSnippet');
		$this->invalidateControl('projectSelectSnippet');
	}
	
	public function handleProjectSelected($value) {
		if ($value) {
			$images = $this->images->findAll()->where('project_id', $value)->fetchPairs('id', 'preview_name');
			if($images) {
				$this->template->images = $images;
				$this->invalidateControl('imagesSnippet');
				$this->invalidateControl('flashesSnippet');
			} else {
				$this->flashMessage('Tento projekt nemá nahraný žádný obrázek', 'error');
				$this->invalidateControl('imagesSnippet');
				$this->invalidateControl('flashesSnippet');
			}
		}
	}
	
	public function handleNewTileSelected($imageId) {
		$this->tiles->changeTile($this->tileId, $imageId);
		$this->flashMessage('Odkaz na hlavní stránce změněn');
		$this->presenter->redirect('Admin:');
	}

}