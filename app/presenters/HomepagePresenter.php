<?php

/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter {

	/** @var App\Model\TileRepository @inject */
	public $tiles;

	/** @var App\Model\ProjectRepository @inject */
	public $projects;

	public function renderDefault() {
		$this->template->tiles = $this->tiles->findAll()->order('id');
	}

	public function renderSitemap() {
		$categoryNames = array(
		    'drobna_architektura',
		    'interier',
		    'rodinne_domy',
		    'obcanske_stavby',
		    'urbanismus'
		);
		$projects = array();
		foreach ($categoryNames as $c) {
			$dbProjects = $this->projects->findByCategory($c);
			$projects[$c] = $dbProjects;
		}
		$this->template->projects = $projects;
	}

}
