<?php

class AdminPresenter extends BasePresenter {

	/** @var IChangePasswordControlFactory @inject */
	public $changePasswordControlFactory;
	
	/** @var IAddProjectControlFactory @inject */
	public $addProjectControlFactory;
	
	/** @var IProjectTableControlFactory @inject */
	public $projectTableControlFactory;
	
	/** @var IChangeTileControlFactory @inject */
	public $changeTileControlFactory;
	
	/** @var App\Model\TileRepository @inject */
	public $tiles;
	
	/** @var int */
	private $tileId;

	protected function startup() {
		parent::startup();
		if (!$this->user->isLoggedIn() && $this->view != 'login') {
			$this->redirect('Admin:login');
		}
	}
	
	public function actionChangeTile($id) {
		$this->tileId = $id;
	}
	
	public function actionOrganizeMainPage() {
		$this->template->tiles = $this->tiles->findAll()->order('id');
	}

	public function handleLogout() {

		$this->user->logout();

		$this->redirect('Homepage:');
	}

	protected function createComponentLogin() {
		return new LoginControl();
	}

	protected function createComponentChangePassword() {
		return $this->changePasswordControlFactory->create();
	}

	protected function createComponentAddProject() {
		return $this->addProjectControlFactory->create();
	}

	protected function createComponentProjectTable() {
		return $this->projectTableControlFactory->create();
	}
	
	function createComponentChangeTileControl() {
		return $this->changeTileControlFactory->create($this->tileId);
	}

}
