<?php

class ProjectPresenter extends BasePresenter {

	/** @var App\Model\ProjectRepository @inject */
	public $projects;

	function beforeRender() {
		parent::beforeRender();
		$this->template->showSubmenu = true;
	}

	public function renderDefault($category) {
		if($category=='') $this->redirect('this', array('category'=>'rodinne_domy'));
		$categories = array(
		    'drobna_architektura' => 'drobná arch.',
		    'interier' => 'interiér',
		    'rodinne_domy' => 'rodinné domy',
		    'obcanske_stavby' => 'občanské stavby',
		    'urbanismus' => 'urbanismus'
		);
		try {
		$this->template->projects = $this->projects->findByCategory($category);
		} catch (Nette\InvalidArgumentException $exc) {
			throw new \Nette\Application\BadRequestException($exc->getMessage());
		}
		$this->template->currentCategory = $categories[$category];
	}

	public function renderDetail($project) {
		if($this->isAjax())
			$this->invalidateControl ('gallery');
		$projects = $this->projects->findById($project);
		$this->template->project = $projects;

		$paginator = $this['visualPaginator']->getPaginator();
		$this['visualPaginator']->showPages(FALSE);
		$paginator->itemsPerPage = 6;
		$paginator->itemCount = $projects->related('images')->count();
		$this->template->offset = $paginator->getOffset();
	}

	public function handleNextProject($projectCategory, $project) {
		$project = $this->projects->findByCategory($projectCategory)->where('id > ?', $project)->fetch();
		if (!$project) {
			$project = $this->projects->findByCategory($projectCategory)->fetch();
		}
		$this->redirect('this', array('category' => $projectCategory, 'project' => $project->id));
	}

	public function handlePreviousProject($projectCategory, $project) {
		$project = $this->projects->findByCategory($projectCategory)->where('id < ?', $project)->order('id DESC')->fetch();
		if (!$project) {
			$project = $this->projects->findByCategory($projectCategory)->order('id DESC')->fetch();
		}
		$this->redirect('this', array('category' => $projectCategory, 'project' => $project->id));
	}

	function createComponentVisualPaginator() {
		return new VisualPaginator();
	}

}